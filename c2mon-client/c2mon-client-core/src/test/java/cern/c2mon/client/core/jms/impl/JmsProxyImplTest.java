/******************************************************************************
 * Copyright (C) 2010-2020 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.client.core.jms.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.jms.*;

import cern.c2mon.client.core.jms.EnqueuingEventListener;
import org.apache.activemq.ActiveMQConnection;
import org.awaitility.Awaitility;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import cern.c2mon.client.core.config.C2monClientProperties;
import cern.c2mon.client.core.config.JmsConfig;
import cern.c2mon.client.core.jms.TopicRegistrationDetails;
import cern.c2mon.client.core.listener.TagUpdateListener;
import cern.c2mon.shared.client.request.ClientRequestResult;
import cern.c2mon.shared.client.request.JsonRequest;

public class JmsProxyImplTest {

  private JmsProxyImpl jmsProxy;

  private JmsConnectionHandler connectionHandler;

  /**
   * Mocks
   */
  ConnectionFactory connectionFactory;
  ActiveMQConnection connection;
  Session session;

  @Before
  public void setUp() {
    connectionFactory = EasyMock.createNiceMock(ConnectionFactory.class);
    connection = EasyMock.createNiceMock(ActiveMQConnection.class);
    session = EasyMock.createNiceMock(Session.class);
    SlowConsumerListener slowConsumerListener = EasyMock.createNiceMock(SlowConsumerListener.class);
    EnqueuingEventListener enqueuingEventListener = EasyMock.createNiceMock(EnqueuingEventListener.class);
    JmsConfig jmsConfig = new JmsConfig();
    ExecutorService topicPollingExecutor = jmsConfig.topicPollingExecutor();
    connectionHandler = new JmsConnectionHandler(connectionFactory, slowConsumerListener, enqueuingEventListener, topicPollingExecutor, new C2monClientProperties());

    jmsProxy = new JmsProxyImpl(connectionHandler, slowConsumerListener, enqueuingEventListener, topicPollingExecutor, new C2monClientProperties());
  }

  /**
   * Call registerUpdateListener with null.
   * @throws JMSException
   */
  @Test(expected = NullPointerException.class)
  public void testRegisterNullUpdateListener() throws JMSException {
    jmsProxy.registerUpdateListener(null, EasyMock.createMock(TopicRegistrationDetails.class));
  }

  @Test(expected = NullPointerException.class)
  public void testRegisterNullDetails() throws JMSException {
    jmsProxy.registerUpdateListener(EasyMock.createMock(TagUpdateListener.class), null);
  }

  @Test(expected = NullPointerException.class)
  public void testUnRegisterNullListener() {
    jmsProxy.unregisterUpdateListener(null);
  }

  @Test
  public void testUnRegisterNullSupervisionListener() {
    jmsProxy.unregisterSupervisionListener(null);
  }

  @Test(expected = NullPointerException.class)
  public void testRegisterNullSupervisionListener() {
    jmsProxy.registerSupervisionListener(null);
  }

  @Test(expected = NullPointerException.class)
  public void testRegisterNullHeartbeatListener() {
    jmsProxy.registerHeartbeatListener(null);
  }

  @Test
  public void testUnregisterNullHeartbeatListener() throws JMSException {
    jmsProxy.unregisterHeartbeatListener(null);
  }

  @Test(expected = NullPointerException.class)
  public void testRegisterNullConnectionListener() throws JMSException {
    jmsProxy.registerConnectionListener(null);
  }

  /**
   * Test sendRequest with null request object - should throw exception.
   * Also calls the lifecycle start() method and checks connection and session
   * calls.
   * @throws JMSException
   */
  @Test(expected = NullPointerException.class)
  public void testStartAndSendRequestNullRequest() throws JMSException {
    //need to simulate start
    MessageConsumer messageConsumer = simulateStart();

    Awaitility.await().atMost(2, TimeUnit.SECONDS)
            .until(() -> connectionHandler.isConnected());

    jmsProxy.sendRequest(null, "test.queue", 1000);
    EasyMock.verify(connectionFactory);
    EasyMock.verify(connection);
    EasyMock.verify(session);
    EasyMock.verify(messageConsumer);
  }

  private MessageConsumer simulateStart() throws JMSException {
    EasyMock.expect(connectionFactory.createConnection()).andReturn(connection).times(4); //  tag, supervision, heartbeat, broadcast
    EasyMock.expect(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).andReturn(session).times(4);
    connection.setExceptionListener(EasyMock.isA(ExceptionListener.class));
    connection.start();

    MessageConsumer messageConsumer = EasyMock.createMock(MessageConsumer.class);
    EasyMock.expect(session.createConsumer(EasyMock.isA(Destination.class))).andReturn(messageConsumer).times(5); // 2 * tag
    messageConsumer.setMessageListener(EasyMock.isA(TagListenerWrapper.class)); // tags
    messageConsumer.setMessageListener(EasyMock.isA(TagListenerWrapper.class)); // control tags
    messageConsumer.setMessageListener(EasyMock.isA(SupervisionListenerWrapper.class));
    messageConsumer.setMessageListener(EasyMock.isA(HeartbeatListenerWrapper.class));
    messageConsumer.setMessageListener(EasyMock.isA(BroadcastMessageListenerWrapper.class));
    session.close();

    EasyMock.replay(connectionFactory);
    EasyMock.replay(connection);
    EasyMock.replay(session);
    EasyMock.replay(messageConsumer);
    connectionHandler.ensureConnection();
    return messageConsumer;
  }

  /**
   * Test sendRequest with null queue name - should throw exception.
   * @throws JMSException
   * @throws InterruptedException
   */
  @Test(expected = NullPointerException.class)
  public void testSendRequestNullQueue() throws JMSException, InterruptedException {
    JsonRequest<ClientRequestResult> jsonRequest = EasyMock.createMock(JsonRequest.class);
    MessageConsumer messageConsumer = simulateStart();
    Thread.sleep(2000); //leave time for connection thread to run (and set connected flag to true)
    jmsProxy.sendRequest(jsonRequest, null, 1000);
    EasyMock.verify(connectionFactory);
    EasyMock.verify(connection);
    EasyMock.verify(session);
    EasyMock.verify(messageConsumer);
  }

}
