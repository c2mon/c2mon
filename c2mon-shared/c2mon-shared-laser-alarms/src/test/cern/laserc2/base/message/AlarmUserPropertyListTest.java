/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.message.AlarmUserPropertyList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { AlarmUserPropertyListTest.class })
public class AlarmUserPropertyListTest {

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmUserPropertyList#toString()}.
     * 
     * Check that we get the DB compatible string representation of a set of user properties.
     */
    @Test
    public void testToString() {
        AlarmUserPropertyList list = new AlarmUserPropertyList();
        assertEquals("", list.toString());
        list.addProperty("A", "B");
        assertEquals("A=B", list.toString());
        list.addProperty("C", "D");
        LoggerFactory.getLogger(AlarmUserPropertyListTest.class).info(">>>{}", list.toString());
        assertEquals("A=B@@C=D", list.toString());
    }

}
