/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.message.AlarmCacheStatusImpl;
import cern.laserc2.base.message.AlarmFaultState;
import cern.laserc2.base.message.AlarmFaultStateImpl;
import cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor;
import cern.laserc2.base.ui.TableDescriptorTest;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TableDescriptorTest.class })
public class AlarmFaultStateTest {

    @Mock
    AlarmCacheStatusImpl status;
    
    /**
     * Test method for {@link cern.laserc2.base.message.AlarmFaultStateImpl#AlarmFaultStateImpl(java.lang.String, java.lang.String, java.lang.String, cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor)}.
     */
    @Test
    public void testAlarmFaultStateImplStringStringStringAlarmFaultStateDescriptor() {
        AlarmFaultStateImpl alarm = new AlarmFaultStateImpl("FF", "FM", "FC", AlarmFaultStateDescriptor.ACTIVE);
        assertEquals("FF", alarm.getFamily());
        assertEquals("FM", alarm.getMember());
        assertEquals("FC", alarm.getCode());
        assertTrue( alarm.isActive());
        assertNotNull(alarm.getProperties());
        assertTrue(0 < alarm.getUserTimeStamp().getMillis());
    }

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmFaultStateImpl#AlarmFaultStateImpl(java.lang.String, cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor)}.
     */
    @Test
    public void testAlarmFaultStateImplStringAlarmFaultStateDescriptor() {
        AlarmFaultStateImpl alarm = new AlarmFaultStateImpl("FF:FM:FC", AlarmFaultStateDescriptor.ACTIVE);
        assertEquals("FF", alarm.getFamily());
        assertEquals("FM", alarm.getMember());
        assertEquals("FC", alarm.getCode());
        assertTrue( alarm.isActive());
        assertNotNull(alarm.getProperties());
        assertTrue(0 < alarm.getUserTimeStamp().getMillis());
    }

    @Test
    public void testCreateFromCache() {

        Map<String,String> props = new HashMap<>();
        props.put("KEY", "VALUE");
        
        given(status.getTriplet()).willReturn("FF:FM:FC");
        given(status.getUserTs()).willReturn(System.currentTimeMillis());
        given(status.getProperties()).willReturn(props);
        
        AlarmFaultState alarm = AlarmFaultState.createFromAlarm(status, AlarmFaultStateDescriptor.ACTIVE);
        assertEquals("FF:FM:FC", alarm.getTriplet());
        assertTrue( alarm.isActive());
        assertNotNull(alarm.getProperties());
        assertTrue(0 < alarm.getUserTs().getMillis());
        assertEquals(1, alarm.getProperties().getProperties().size());
    }

}
