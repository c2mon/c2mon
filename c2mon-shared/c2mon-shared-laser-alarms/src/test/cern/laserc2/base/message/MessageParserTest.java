/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.io.IOException;

import cern.laserc2.base.message.AlarmMessage;
import cern.laserc2.base.message.AlarmMessageFactory;

public class MessageParserTest {

    //@formatter:off
    static String message = "<?xml version='1.0' encoding='ASCII'?> "
            + "<ASI-message version='0.9' backup='false'  keep-alive='false'> "
            + "    <source-name>LASER-CHECK-CONSOLES</source-name> "
            + "<source-hostname>x</source-hostname> "
            + "    <source-hostname>CWE-513-VOL717</source-hostname> "
            + "    <source-timestamp seconds='1517580714' microseconds='482107'/> " 
            + "    <fault-states> " 
            + "         <fault-state  family='LASER' member='CHECK_CONSOLES' code='1'> "
            + "              <descriptor>TERMINATE</descriptor> " + "     <user-properties> "
            + "              </user-properties> " 
            + "              <user-timestamp seconds='1517580714' microseconds='479000'/> " 
            +"        </fault-state> " 
            + "    </fault-states> " 
            + "</ASI-message>";
    //@formatter:on

    public static void main(String[] args) throws IOException {
        AlarmMessage alarmMessage = AlarmMessageFactory.getFactory().createMessagefromXml(message);
        System.out.println(" >>> " +alarmMessage.getSourceHostname());
        System.out.println(" >>> " +alarmMessage.getSourceId());
    }
    
}
