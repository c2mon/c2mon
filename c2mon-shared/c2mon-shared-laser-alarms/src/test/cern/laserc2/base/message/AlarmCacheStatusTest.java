/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.message.AlarmCacheStatus;
import cern.laserc2.base.message.AlarmCacheStatusImpl;
import cern.laserc2.base.ui.TableDescriptorTest;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TableDescriptorTest.class })
public class AlarmCacheStatusTest {

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmCacheStatusImpl#AlarmCacheStatusImpl(java.lang.String)}.
     */
    @Test
    public void testAlarmCacheStatusImpl() {
        AlarmCacheStatus status = AlarmCacheStatus.createCacheStatus("FF",  "FM",  "1000");
        assertEquals("FF:FM:1000", status.getTriplet());
    }



    /**
     * Test method for {@link cern.laserc2.base.message.AlarmCacheStatusImpl#setActive(boolean)}.
     * @throws InterruptedException 
     */
    @Test
    public void testSetActive() throws InterruptedException {
        AlarmCacheStatusImpl status = new AlarmCacheStatusImpl("FF:FM:1000");
        assertEquals(0, status.getSystemTs());
        status.setActive(true);
        assertTrue(status.isActive());
        assertTrue(0 < status.getSystemTs());
        long ts = status.getSystemTs();

        // the alarm is already true, it should not change
        status.setActive(true);
        assertTrue(status.isActive());
        assertEquals(ts, status.getSystemTs());
        
        // a change should again increase systemTs
        // ... make sure the system has time to increase a bit the ts
        Thread.sleep(10);
        status.setActive(false);
        assertFalse(status.isActive());
        assertTrue(ts < status.getSystemTs());
    }


    /**
     * Test method for {@link cern.laserc2.base.message.AlarmCacheStatusImpl#getProperties()}.
     */
    @SuppressWarnings("serial")
    @Test
    public void testGetProperties() {
        AlarmCacheStatusImpl status = new AlarmCacheStatusImpl("FF:FM:1000");
        status.setProperty("KEY",  "VALUE");
        assertEquals("VALUE", status.getProperties().get("KEY"));
        
        status.setProperties(null);
        assertNotNull(status.getProperties());
        assertNull(status.getProperties().get("KEY"));
        
        status.setProperties(new HashMap<String, String>() {{
            put("KEY","VALUE");
        }});
        assertEquals("VALUE", status.getProperties().get("KEY"));
    }


}
