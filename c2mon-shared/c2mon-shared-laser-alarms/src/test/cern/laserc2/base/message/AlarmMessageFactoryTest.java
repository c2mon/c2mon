/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.message.AlarmFaultState;
import cern.laserc2.base.message.AlarmFaultStateImpl;
import cern.laserc2.base.message.AlarmMessage;
import cern.laserc2.base.message.AlarmMessageFactory;
import cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor;
import cern.laserc2.base.ui.TableDescriptorTest;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TableDescriptorTest.class })
public class AlarmMessageFactoryTest {

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmMessageFactoryImpl#alarmMessageToXml(cern.laserc2.base.message.AlarmMessage)}.
     * @throws IOException 
     */
    @Test
    public void testAlarmMessageToXml() throws IOException {
        AlarmMessageFactory messageFactory = AlarmMessageFactory.getFactory();
        InputStream xmlStream = AlarmMessageFactoryTest.class.getResourceAsStream("/testdata.xml");
        String xmlBeforeTransfo = new BufferedReader(new InputStreamReader(xmlStream))
                .lines().collect(Collectors.joining("\n"));
        AlarmMessage message = messageFactory.createMessagefromXml(xmlBeforeTransfo);
        String xmlAfterTransfo = messageFactory.alarmMessageToXml(message);
        
        assertTrue(xmlAfterTransfo.contains("<source-name>LASER-CHECK-CONSOLES</source-name>"));
        assertTrue(xmlAfterTransfo.contains("<source-hostname>cwe-513-vol175</source-hostname>"));
        assertTrue(xmlAfterTransfo.contains("<fault-state family=\"LASER\" member=\"CHECK_CONSOLES\" code=\"1\">"));
        assertTrue(xmlAfterTransfo.contains("<property name=\"ASI_EVENT_ID\" value=\"1\"/>"));
        assertTrue(xmlAfterTransfo.contains("<user-timestamp seconds=\"1455621361\" microseconds=\"891417\"/>"));
        }

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmMessageFactoryImpl#createMessagefromXml(java.lang.String)}.
     * @throws IOException 
     */
    @Test
    public void testCreateMessagefromXml() throws IOException {
        AlarmMessageFactory messageFactory = AlarmMessageFactory.getFactory();
        InputStream xmlStream = AlarmMessageFactoryTest.class.getResourceAsStream("/testdata.xml");
        String xml = new BufferedReader(new InputStreamReader(xmlStream))
                .lines().collect(Collectors.joining("\n"));
        AlarmMessage message = messageFactory.createMessagefromXml(xml);
        
        assertEquals("LASER-CHECK-CONSOLES", message.getSourceId());
        assertEquals(1, message.getFaultStates().getFaultStates().size());
        assertFalse(message.isBackup());
        assertEquals(1455621372400L, message.getSourceTimeStamp().getMillis());    
        
        for (AlarmFaultState fs : message.getFaultStates().getFaultStates()) {
            assertEquals("LASER:CHECK_CONSOLES:1", fs.getTriplet());    
            assertEquals(1455621361891L, fs.getUserTs().getMillis());    
            assertFalse(fs.isActive());
        }

    }

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmMessageFactoryImpl#createMessage(java.lang.String, boolean)}.
     */
    @Test
    public void testCreateMessage() {
        AlarmMessageFactory messageFactory = AlarmMessageFactory.getFactory();
        AlarmMessage message = messageFactory.createMessage("TEST_SOURCE", false);
        AlarmFaultState afs = new AlarmFaultStateImpl("FF", "FM", "1000", AlarmFaultStateDescriptor.ACTIVE);
        afs.setUserTs(System.currentTimeMillis());
        afs.addUserProperty("ASI_SUFFIX", "test suffix");
        message.addFaultState(afs);
        
        assertEquals("TEST_SOURCE", message.getSourceId());
        assertEquals(1, message.getFaultStates().getFaultStates().size());
        assertFalse(message.isBackup());
        
        for (AlarmFaultState fs : message.getFaultStates().getFaultStates()) {
            assertEquals("FF:FM:1000", fs.getTriplet());    
        }
        
    }

}
