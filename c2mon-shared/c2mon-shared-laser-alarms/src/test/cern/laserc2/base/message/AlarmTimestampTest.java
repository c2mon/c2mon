/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.message.AlarmTimestamp;
import cern.laserc2.base.ui.TableDescriptorTest;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TableDescriptorTest.class })
public class AlarmTimestampTest {

    /**
     * Test method for {@link cern.laserc2.base.message.AlarmTimestamp#getMillis()}.
     * 
     * Check that we get back the millis from construction, i.e. that the conversion into seconds and
     * micros does not affect the real date represented by the long value.
     */
    @Test
    public void testGetMillis() {
        long ts = System.currentTimeMillis();
        AlarmTimestamp ats = new AlarmTimestamp(ts);
        assertEquals(ts, ats.getMillis());
    }

}
