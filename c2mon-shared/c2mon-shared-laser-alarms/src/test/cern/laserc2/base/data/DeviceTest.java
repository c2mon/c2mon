/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.data.AlarmHandle;
import cern.laserc2.base.data.Device;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { DeviceTest.class})
public class DeviceTest {

    private static Device device = new Device("DEVICE_CLASS", "DEVICE_NAME");

    @Mock
    AlarmHandle alarm;

    
    /**
     * Test method for {@link cern.laserc2.base.data.Device#Device(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testDevice() {
        assertEquals("DEVICE_CLASS", device.getDeviceClass());
        assertEquals("DEVICE_NAME", device.getDeviceName());
        assertNotNull(device.getFaults());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Device#addAlarm(cern.laserc2.base.data.AlarmHandle)}.
     */
    @Test
    public void testAddAlarm() {
        given(alarm.getFaultCode()).willReturn("1");
        device.addAlarm(alarm);
        assertTrue(device.getFaults().contains(alarm));
        device.removeAlarm(alarm);
        assertFalse(device.getFaults().contains(alarm));
    }

}
