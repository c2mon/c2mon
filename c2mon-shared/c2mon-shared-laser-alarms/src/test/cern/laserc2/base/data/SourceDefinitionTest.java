/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.data.SourceDefinition;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { SourceDefinitionTest.class})
public class SourceDefinitionTest {

    private static SourceDefinition sd;
    
    @Before
    public void initSource() {
        sd = new SourceDefinition("TEST_SOURCE", 30);        
    }
    
    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#SourceDefinition(java.lang.String, int)}.
     */
    @Test
    public void testSourceDefinition() {
        assertEquals("TEST_SOURCE", sd.getSourceId());
        assertEquals(30, sd.getBackupTimeout());
        assertFalse(sd.isBackupMissing());
        assertEquals(0, sd.getLastBackupTs());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#receivedMessage()}.
     */
    @Test
    public void testReceivedMessage() {
        long testStartTime = System.currentTimeMillis();
        sd.receivedMessage();
        assertTrue(sd.getLastMessageTs() >= testStartTime);
    }


    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#receivedBackup(java.lang.String)}.
     */
    @Test
    public void testReceivedBackup() {
        long testStartTime = System.currentTimeMillis();
        sd.receivedBackup("cs-ccr-laser2.cern.ch");
        assertEquals("cs-ccr-laser2.cern.ch", sd.getSourceHost());
        assertTrue(sd.getLastBackupTs() >=  testStartTime);
    }

    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#checkForMissingBackup()}.
     */
    @Test
    public void testCheckForMissingBackup() throws Exception {
        sd.receivedBackup("cs-ccr-laser2");
        Thread.sleep(30 * 1000);
        assertFalse(sd.checkForMissingBackup());
        Thread.sleep(5 * 1000);
        assertTrue(sd.checkForMissingBackup());
        sd.receivedBackup("cs-ccr-laser2");
        assertFalse(sd.checkForMissingBackup());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#receivedKeepAlive()}.
     */
    @Test
    public void testReceivedKeepAlive() {
        long testStartTime = System.currentTimeMillis();
        sd.receivedKeepAlive();
        assertTrue(sd.getLastKeepAliveTs() >= testStartTime);
    }

    /**
     * Test method for {@link cern.laserc2.base.data.SourceDefinition#checkForMissingKeepAlive()}.
     */
    @Test
    public void testCheckForMissingKeepAlive() throws Exception{
        sd.setKeepAliveDelay(15);
        sd.receivedKeepAlive();
        Thread.sleep(10 * 1000);
        assertFalse(sd.checkForMissingKeepAlive());
        Thread.sleep(10 * 1000);
        assertTrue(sd.checkForMissingKeepAlive());
        sd.receivedKeepAlive();
        assertFalse(sd.checkForMissingKeepAlive());
    }

}
