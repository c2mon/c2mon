/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.data.ClientAlarmEvent;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { DeviceTest.class })
public class ClientAlarmEventTest {

    /**
     * Test method for {@link cern.laserc2.base.data.ClientAlarmEvent#getProblemDescription()}.
     */
    @Test
    public void testGetProblemDescription() {
        ClientAlarmEvent event = new ClientAlarmEvent();
        event.setPbDesc("problem description");
        assertTrue(0 > event.getProblemDescription().indexOf("["));
        assertTrue(0 > event.getProblemDescription().indexOf("]"));
        
        Map<String,String> map = new HashMap<>();
        map.put("ASI_PREFIX", "OSC");
        event.setUserProps(map);
        assertTrue(0 <= event.getProblemDescription().indexOf("[OSC] problem description"));
        
        map.put("ASI_SUFFIX", "PSB.USER.ZERO");
        event.setUserProps(map);
        assertTrue(0 <= event.getProblemDescription().indexOf("[OSC] problem description [PSB.USER.ZERO]"));

    }

    /**
     * Test method for {@link cern.laserc2.base.data.ClientAlarmEvent#toStringFilter()}.
     */
    @Test
    public void testToStringFilter() {
        ClientAlarmEvent event = new ClientAlarmEvent();
        event.setFaultFamily("FF");
        event.setFaultMember("FM");
        event.setSystem("Sys");
        event.setIdentifier("Identifier");
        event.setPbDesc("Problem description");
        
        assertTrue(0 <= event.toStringFilter().indexOf("FF"));
        assertTrue(0 <= event.toStringFilter().indexOf("FM"));
        assertTrue(0 <= event.toStringFilter().indexOf("ys"));
        assertTrue(0 <= event.toStringFilter().indexOf("fier"));
        assertTrue(0 <= event.toStringFilter().indexOf("ys Id"));
        assertTrue(0 <= event.toStringFilter().indexOf("lem des"));
    }

}
