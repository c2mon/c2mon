/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.data.Triplet;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TripletTest.class})

public class TripletTest {

    private Triplet triplet;

    @Before
    public void initTestTriplet() {
        triplet = Triplet.create("FF" ,"FM", 1);
    }
    
    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#getFaultFamily()}.
     */
    @Test
    public void testGetFaultFamily() {
        assertEquals("FF", triplet.getFaultFamily());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#getFaultMember()}.
     */
    @Test
    public void testGetFaultMember() {
        assertEquals("FM", triplet.getFaultMember());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#getFaultCode()}.
     */
    @Test
    public void testGetFaultCode() {
        assertEquals("1", triplet.getFaultCode());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#toString()}.
     */
    @Test
    public void testToString() {
        assertEquals("FF:FM:1", triplet.toString());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#create(java.lang.String)}.
     */
    @Test
    public void testCreateString() {
        assertEquals("FF:FM:1", Triplet.create("FF:FM:1").toString());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#create(java.lang.String, java.lang.String, int)}.
     */
    @Test
    public void testCreateStringStringInt() {
        assertEquals("FF:FM:1", Triplet.create("FF","FM", 1).toString());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#create(java.lang.String, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testCreateStringStringString() {
        assertEquals("FF:FM:1", Triplet.create("FF","FM", "1").toString());
    }

    /**
     * Test method for {@link cern.laserc2.base.data.Triplet#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
        assertEquals(Triplet.create("FF:FM:1"), Triplet.create("FF","FM", 1));
    }

}
