/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.oscillation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import cern.laserc2.base.oscillation.OscillationController;
import cern.laserc2.base.oscillation.OscillationData;

public class OscillationControllerTest {

    @Test
    public void testOsc() {
        OscillationController controller = new OscillationController();
        controller.oscillationControlEnabled = true;

        // start test 6 hours ago
        TestClock clock = new TestClock(System.currentTimeMillis() - OscillationData.ONE_HOUR * 6);
        System.out.println("Starting simulation at:" + clock.dateAsStr());
        OscillationData oscd = controller.recordEvent("FF:FM:1", clock.getMillis());
        assertFalse(oscd.isOscillating());

        for (int i = 0; i < 10; i++) {
            controller.recordEvent("FF:FM:2", clock.incrSeconds(2));
            controller.recordEvent("FF:FM:2", clock.incrSeconds(2));
            controller.recordEvent("FF:FM:2", clock.incrSeconds(2));
            oscd = controller.recordEvent("FF:FM:1", clock.incrSeconds(2));
            assertFalse(oscd.isOscillating());
        }
        System.out.println(clock.dateAsStr() + ": " + oscd.isOscillating());
        for (int i = 0; i < 10; i++) {
            controller.recordEvent("FF:FM:1", clock.incrSeconds(2));
        }
        System.out.println(clock.dateAsStr() + ": " + oscd.isOscillating());
        assertTrue(oscd.isOscillating());
        if (!oscd.isOscillationNotified()) {
            System.out.println("Writing down the oscillation status ...");
            controller.setOscillationNotified(oscd, true);
        }

        oscd = controller.recordEvent("FF:FM:1", clock.incrSeconds(30 * 60));
        assertTrue(oscd.isOscillating());
        assertTrue(oscd.isOscillationNotified());

        System.out.println(clock.dateAsStr() + ": " + oscd.isOscillating());
        oscd = controller.recordEvent("FF:FM:1", clock.incrSeconds(30 * 60));
        System.out.println(clock.dateAsStr() + ": " + oscd.isOscillating());
        assertFalse(oscd.isOscillating());
        System.out.println(clock.dateAsStr() + ": " + oscd.isOscillating());

        for (int i = 0; i < 120; i++) {
            oscd = controller.recordEvent("FF:FM:1", clock.incrSeconds(2));
        }
        assertTrue(oscd.isOscillating());
        assertFalse(oscd.isOscillationNotified());
        System.out.println("Done.");
    }

}
