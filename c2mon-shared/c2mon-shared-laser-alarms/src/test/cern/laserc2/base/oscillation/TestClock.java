/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.oscillation;

import java.util.Date;

public class TestClock {
    private long ts;

    public TestClock(long initialTsMillis) {
        this.ts = initialTsMillis;
    }

    public long incrSeconds(int s) {
        ts = ts + s * 1000;
        return ts;
    }

    public String dateAsStr() {
        return new Date(ts).toString();
    }

    public long getMillis() {
        return ts;
    }
}
