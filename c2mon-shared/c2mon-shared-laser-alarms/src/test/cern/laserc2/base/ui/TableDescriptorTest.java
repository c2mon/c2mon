/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.ui;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cern.laserc2.base.data.AlarmHandle;
import cern.laserc2.base.ui.ColumnDescriptor;
import cern.laserc2.base.ui.TableDescriptor;
import cern.laserc2.base.ui.TableDescriptor.Column;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TableDescriptorTest.class })
public class TableDescriptorTest {

    TableDescriptor tableDescriptor = TableDescriptor.getDefaultDescriptor();

    @Mock
    AlarmHandle alarm;

    /**
     * Test method for {@link cern.laserc2.base.ui.TableDescriptor#getColumnNames()}.
     */
    @Test
    public void testGetColumnNames() {
        assertTrue(tableDescriptor.getColumnNames().contains(Column.BUILDING.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.DATE_TIME.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.SYSTEM_NAME.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.IDENTIFIER.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.FAULT_CODE.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.PROBLEM_DESCRIPTION.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.PRIORITY.toString()));
        assertTrue(tableDescriptor.getColumnNames().contains(Column.SOURCE_ID.toString()));
    }

    /**
     * Test method for
     * {@link cern.laserc2.base.ui.TableDescriptor#setColumn(cern.laserc2.base.ui.TableDescriptor.Column, java.lang.String, boolean, int, int)}
     * .
     */
    @Test
    public void testSetGetColumns() {
        tableDescriptor.setColumn(Column.BUILDING, "TestLabel", true, 100, 100);
        ColumnDescriptor lastCol = tableDescriptor.getColumns().stream().reduce((a, b) -> b).orElse(null);
        assertEquals("TestLabel", lastCol.getLabel());
        int numberOfColumnsBefore = tableDescriptor.getColumns().size();
        tableDescriptor.setColumn(Column.BUILDING, "TestLabel", false, 100, 100);
        assertTrue(numberOfColumnsBefore > tableDescriptor.getColumns().size());
    }

    /**
     * Test method for {@link cern.laserc2.base.ui.TableDescriptor#isDisplayed(java.lang.String)}.
     */
    @Test
    public void testIsDisplayed() {
        assertTrue(tableDescriptor.isDisplayed(Column.DATE_TIME.toString()));
        tableDescriptor.setColumn(Column.DATE_TIME, "test", false, 100, 100);
        assertFalse(tableDescriptor.isDisplayed(Column.DATE_TIME.toString()));
    }

    /**
     * Test method for
     * {@link cern.laserc2.base.ui.TableDescriptor#show(java.lang.String, cern.laserc2.base.data.AlarmHandle)}.
     */
    @Test
    public void testShow() {
        given(alarm.getLocation()).willReturn("774");
        given(alarm.getSourceId()).willReturn("TEST_SOURCE");
        given(alarm.getSystem()).willReturn("FGC_61");
        given(alarm.getUserTs()).willReturn(1500000000000L);
        given(alarm.getFaultCode()).willReturn("1000");
        given(alarm.getPriority()).willReturn(2);
        given(alarm.getHelpURL()).willReturn("http://wikis.cern.ch");
        given(alarm.getIdentifier()).willReturn("id");

        assertEquals("774", tableDescriptor.show(Column.BUILDING.toString(), alarm));
        assertEquals("TEST_SOURCE", tableDescriptor.show(Column.SOURCE_ID.toString(), alarm));
        assertEquals("FGC_61", tableDescriptor.show(Column.SYSTEM_NAME.toString(), alarm));
        assertEquals("14-07-2017 04:40:00", tableDescriptor.show(Column.DATE_TIME.toString(), alarm));
        assertEquals("2", tableDescriptor.show(Column.PRIORITY.toString(), alarm));
        assertEquals("1000", tableDescriptor.show(Column.FAULT_CODE.toString(), alarm));
        assertEquals("<a href=\"http://wikis.cern.ch\" target=_blank>id</a>",
                tableDescriptor.show(Column.IDENTIFIER.toString(), alarm));

        given(alarm.getAlarmId()).willReturn(1000L);
        given(alarm.getPrefix()).willReturn("OSC");
        given(alarm.getSuffix()).willReturn("PSB.USER.ZERO");
        given(alarm.getProblemDescription()).willReturn("Problem description");
        assertEquals("<a href=\"/details/alarm?alarm=1000\">OSC Problem description PSB.USER.ZERO</a>",
                tableDescriptor.show(Column.PROBLEM_DESCRIPTION.toString(), alarm));
        
    }

}
