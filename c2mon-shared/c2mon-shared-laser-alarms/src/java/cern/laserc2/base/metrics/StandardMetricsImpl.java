/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.metrics;

import java.util.Date;

import org.springframework.jmx.export.annotation.ManagedOperation;


public class StandardMetricsImpl implements StandardMetrics {

    private volatile long errorCounter;
    private volatile long warningCounter;
    private volatile long processedCounter;

    private volatile long lastErrorTs;
    private volatile long lastWarningTs;
    private volatile long lastProcessedTs;
    private String lastErrorMessage;
    private String lastWarningMessage;

    //
    // --- implements StandardMetrics
    //
    @Override
    @ManagedOperation
    public void reset() {
        errorCounter = 0;
        processedCounter = 0;
        warningCounter =0;
        lastErrorMessage = "";                
        lastWarningMessage = "";                
        lastErrorTs = 0;
        lastWarningTs = 0;
        lastProcessedTs = 0;
    }
    
    @Override
    public long getErrorCounter() {
        return errorCounter;
    }

    @Override
    public void setErrorCounter(long errorCounter) {
        this.errorCounter = errorCounter;
    }

    @Override
    public long getWarningCounter() {
        return warningCounter;
    }

    @Override
    public long getProcessedCounter() {
        return processedCounter;
    }
    
    @Override
    public String getLastWarningMessage() {
        return lastWarningMessage;
    }

    public void setProcessedCounter(long processedCounter) {
        this.processedCounter = processedCounter;
    }

    @Override
    public synchronized String getLastErrorMessage() {
        return lastErrorMessage;
    }

    @Override
    public void increaseProcessedCounter() {
        increaseProcessedCounter(true);
    }
    
    @Override
    public void increaseProcessedCounter(boolean updateLastProcessedTs) {
        processedCounter++;
        if (updateLastProcessedTs) {
            this.lastProcessedTs = System.currentTimeMillis();
        }
    }
    

    @Override
    public void increaseWarningCounter() {
        warningCounter++;
        this.lastWarningTs = System.currentTimeMillis();
    }
    
    @Override
    public void increaseWarningCounter(@SuppressWarnings("hiding") String lastWarningMessage) {
        increaseWarningCounter();
        this.lastWarningMessage = lastWarningMessage;
    }

    @Override
    public void increaseErrorCounter() {
        errorCounter++;
        this.lastErrorTs = System.currentTimeMillis();
    }

    @Override
    public synchronized void increaseErrorCounter(@SuppressWarnings("hiding") String lastErrorMessage) {
        increaseErrorCounter();
        this.lastErrorMessage = lastErrorMessage;
    }
    
    public void setWarningCounter(long warningCounter) {
        this.warningCounter = warningCounter;
    }

    @Override
    public String getLastErrorDateStr() {
        return (new Date(lastErrorTs)).toString();
    }

    @Override
    public String getLastWarningDateStr() {
        return (new Date(lastWarningTs)).toString();
    }

    @Override
    public String getLastProcessedDateStr() {
        return (new Date(lastProcessedTs)).toString();
    }
    
}
