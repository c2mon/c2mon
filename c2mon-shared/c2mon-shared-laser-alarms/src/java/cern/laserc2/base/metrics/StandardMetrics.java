/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.metrics;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource
public interface StandardMetrics {
    
    //
    //--- JMX operations and attributes ------------------
    // 
    @ManagedOperation
    void reset();
    
    @ManagedOperation
    void setErrorCounter(long errorCounter);

    @ManagedAttribute
    long getErrorCounter();

    @ManagedAttribute
    long getWarningCounter();

    @ManagedAttribute
    long getProcessedCounter();

    @ManagedAttribute
    String getLastWarningMessage();

    @ManagedAttribute
    String getLastErrorMessage();
    
    @ManagedAttribute
    String getLastProcessedDateStr();
    
    @ManagedAttribute
    String getLastWarningDateStr();
    
    @ManagedAttribute
    String getLastErrorDateStr();
    
    //
    // --- API for client applications -------------------------------------------
    //
    void increaseProcessedCounter();
    void increaseProcessedCounter(boolean updateLastProcessedTs);   // for performance, give possibility to NOT update ts
    void increaseWarningCounter();
    void increaseWarningCounter(String lastWarningMessage);
    void increaseErrorCounter();
    void increaseErrorCounter(String lastErrorMessage);



}
