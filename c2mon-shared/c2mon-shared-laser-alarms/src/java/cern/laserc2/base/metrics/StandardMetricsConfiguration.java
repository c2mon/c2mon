/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.metrics;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StandardMetricsConfiguration {

    @Bean
    StandardMetrics metrics() {
        return new StandardMetricsImpl();
    }
}
