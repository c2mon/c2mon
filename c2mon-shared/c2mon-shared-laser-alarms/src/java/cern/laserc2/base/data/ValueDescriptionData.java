/**
 * Copyright (c) 2022 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ValueDescriptionData {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValueDescriptionData.class);

    private static final String ALARM_PREFIX_FIELD = "alarm_prefix";
    private static final String ALARM_SUFFIX_FIELD = "alarm_suffix";
    private static final String ALARM_TIMESTAMP_FIELD = "alarm_timestamp";
    private static final String ACTIVE_USERS_FIELD = "alarm_user";
    private static final String ALARM_PB_DESC = "alarm_pb_desc";

    // used for concatenation wih StringBuilder , should never be null
    private String prefix = "";
    private String suffix = "";

    private StringBuilder activeUsers = new StringBuilder();
    private long userTs;

    private String problemDescription = "";
    private boolean hasProblemDescription;

    public ValueDescriptionData(String valueDescription) {

        if (valueDescription != null && !valueDescription.isEmpty()) {
            String json= valueDescription;
            if (valueDescription.indexOf("Communication fault tag indicates that equipment") >=0) {
                int reasonIndex = valueDescription.indexOf("Reason:");
                if ( reasonIndex >= 0) {
                    json = valueDescription.substring(reasonIndex + 8);
                }
            }
            
            try {
                JsonElement rootElement = new JsonParser().parse(json);
                JsonObject jsonObject = rootElement.getAsJsonObject();

                JsonElement prefixElem = jsonObject.get(ALARM_PREFIX_FIELD);
                if (prefixElem != null) {
                    this.prefix = prefixElem.getAsString();
                }

                JsonElement suffixElem = jsonObject.get(ALARM_SUFFIX_FIELD);
                if (suffixElem != null) {
                    this.suffix = suffixElem.getAsString();
                }

                JsonElement pbDescElem = jsonObject.get(ALARM_PB_DESC);
                if (pbDescElem != null) {
                    this.problemDescription = pbDescElem.getAsString();
                    if (!problemDescription.isEmpty()) {
                        LOGGER.debug("ValueDescription: {}", valueDescription);
                        this.hasProblemDescription = true;
                    }
                }

                JsonElement userTsElem = jsonObject.get(ALARM_TIMESTAMP_FIELD);
                if (userTsElem != null) {
                    this.userTs = userTsElem.getAsLong();
                }

                JsonElement activeUsersElem = jsonObject.get(ACTIVE_USERS_FIELD);
                if (activeUsersElem != null) {
                    JsonArray activeUsersJsonArray = activeUsersElem.getAsJsonArray();
                    for (int i = 0; i < activeUsersJsonArray.size(); i++) {
                        if (i > 0) {
                            this.activeUsers.append(",");
                        }
                        this.activeUsers.append(activeUsersJsonArray.get(i).getAsString());
                    }
                }
            } catch (Exception e) {
                // Most likely this is just because there is no JSON in the description
                LOGGER.debug("Failed to parse JSON value description ({})", json);
            }
        }
    }

    @Override
    public String toString() {
        return "ValueDescriptionDecoder [pbDesc=" + problemDescription + ", prefix=" + prefix + ", suffix=" + suffix
                + ", activeUsers=" + activeUsers + ", userTs=" + userTs + "]";
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getActiveUsers() {
        return activeUsers.toString();
    }

    public long getUserTs() {
        return userTs;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public boolean hasProblemDescription() {
        return hasProblemDescription;
    }

    public String toDisplayString() {
        if (activeUsers.toString().isEmpty()) {
            return "";
        }
        return " ActiveUsers=" + activeUsers;
    }

}
