/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

/**
 * The alarm system nominates responsible persons for sources and for alarm definitions. This information is used in the
 * import procedure, the server for the web interface and the swing UI (details dialog)
 * 
 * @author mbuttner
 */
public class Responsible {
    private int cernId;
    private String fullname;
    private String organicUnit;
    private String email;
    private String phones;

    //
    // --- CONSTRUCTION --------------------------------------------------------------------
    //
    public Responsible(int cernId) {
        this.cernId = cernId;
    }

    //
    // --- PUBLIc METHODS ------------------------------------------------------------------
    //
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getOrganicUnit() {
        return organicUnit;
    }

    public void setOrganicUnit(String organicUnit) {
        this.organicUnit = organicUnit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public int getCernId() {
        return cernId;
    }
}
