/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

import java.util.Collection;
import java.util.HashMap;

/**
 * The device keep track of its own fault list, so that it is possible to simply replace the list of active faults by
 * another list. This is useful for the monitoring of FESA devices, which do publish a list of active fault codes. At
 * each update, it is required to compare what is no longer active, what stays active (no message!) and what became
 * active. The device class avoids this logic to be implemented in user code. Note that the interactions with the faults
 * can not be done directly on the device instance. The application must retrieve a reference to the device, and than
 * ask the @see Source ( in charge of the devices alarms) to clear all faults for the device, or replace with a new
 * list. Inidividual alarm actions are automatically applied to the corresponding device.
 *
 * @author mbuttner
 */
public class Device {
    public enum DeviceType {
        FESA,
        GM,
        TEST
    }

    private String deviceClass;
    private String deviceName;

    private String hostName;
    private DeviceType devType;

    private HashMap<Integer, AlarmHandle> activeFaults;

    //
    // --- CONSTRUCTION ------------------------------------------------------------------------
    //
    public Device(String deviceClass, String deviceName) {
        this.deviceClass = deviceClass;
        this.deviceName = deviceName;
        activeFaults = new HashMap<Integer, AlarmHandle>();
    }

    //
    // --- PUBLIC METHODS -----------------------------------------------------------------------
    //
    public String getDeviceClass() {
        return deviceClass;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public Collection<AlarmHandle> getFaults() {
        return this.activeFaults.values();
    }

    public void addAlarm(AlarmHandle alarm) {
        activeFaults.put(new Integer(alarm.getFaultCode()), alarm);
    }

    public void removeAlarm(AlarmHandle alarm) {
        activeFaults.remove(new Integer(alarm.getFaultCode()));
    }

    public DeviceType getDeviceType() {
        return this.devType;
    }

    public void setDeviceType(DeviceType devType) {
        this.devType = devType;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

}
