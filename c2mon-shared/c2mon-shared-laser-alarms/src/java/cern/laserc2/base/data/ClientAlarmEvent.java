package cern.laserc2.base.data;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class ClientAlarmEvent implements AlarmHandle {
    
    private long eventId;

    private String faultFamily;
    private String faultMember;
    private String faultCode;

    private String sourceId;

    private String system;
    private String identifier;
    private String pbDesc;

    private int priority;
    private int responsibleId;

    private String cause;
    private String action;
    private String consequence;
    private String helpUrl;

    private String location;
    private String modeMask;
    
    // status assigned by current user
    private int configId;
    private boolean newFlag;

    private boolean acknowledged;
    private boolean masked;
    private boolean highlighted;
    private boolean inhibited;
    private boolean latched;

    // alarm status
    private Map<String, String> userProps = new HashMap<>();

    private long sourceTs;
    private long userTs;
    private long systemTs;

    private long eventSeq;

    private Descriptor descriptor;
    private boolean active;
    private boolean byBackup;
    private long alarmId;

    public String getFaultFamily() {
        return faultFamily;
    }

    public void setFaultFamily(String faultFamily) {
        this.faultFamily = faultFamily;
    }

    public String getFaultMember() {
        return faultMember;
    }

    public void setFaultMember(String faultMember) {
        this.faultMember = faultMember;
    }

    @Override
    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    @Override
    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    @Override
    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getPbDesc() {
        return pbDesc;
    }

    public void setPbDesc(String pbDesc) {
        this.pbDesc = pbDesc;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public int getResponsibleId() {
        return responsibleId;
    }

    public void setResponsibleId(int responsibleId) {
        this.responsibleId = responsibleId;
    }

    @Override
    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Override
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String getConsequence() {
        return consequence;
    }

    public void setConsequence(String consequence) {
        this.consequence = consequence;
    }

    @Override
    public String getHelpURL() {
        return this.helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    @Override
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    @Override
    public boolean isNewFlag() {
        return newFlag;
    }

    @Override
    public void setNewFlag(boolean newFlag) {
        this.newFlag = newFlag;
    }

    @Override
    public boolean isAcknowledged() {
        return acknowledged;
    }

    @Override
    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    @Override
    public boolean isMasked() {
        return masked;
    }

    public void setMasked(boolean masked) {
        this.masked = masked;
    }

    @Override
    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    @Override
    public boolean isInhibited() {
        return inhibited;
    }

    public void setInhibited(boolean inhibited) {
        this.inhibited = inhibited;
    }

    @Override
    public boolean isLatched() {
        return latched;
    }

    public void setLatched(boolean latched) {
        this.latched = latched;
    }

    public Map<String, String> getUserProps() {
        return userProps;
    }

    public void setUserProps(Map<String, String> map) {
        this.userProps = map;
    }

    @Override
    public long getSourceTs() {
        return sourceTs;
    }

    public void setSourceTs(long sourceTs) {
        this.sourceTs = sourceTs;
    }

    public void setUserTs(long userTs) {
        this.userTs = userTs;
    }

    @Override
    public long getSystemTs() {
        return systemTs;
    }

    public void setSystemTs(long systemTs) {
        this.systemTs = systemTs;
    }

    public long getEventSeq() {
        return eventSeq;
    }

    public void setEventSeq(long eventSeq) {
        this.eventSeq = eventSeq;
    }

    public Descriptor getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(Descriptor descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isByBackup() {
        return byBackup;
    }

    public void setByBackup(boolean byBackup) {
        this.byBackup = byBackup;
    }

    @Override
    public String getDeviceClass() {
        return faultFamily;
    }

    @Override
    public String getDeviceName() {
        return faultMember;
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    @Override
    public long getAlarmId() {
        return this.alarmId;
    }

    @Override
    public String getProblemDescription() {
        StringBuilder retval = new StringBuilder();
        String prefix = userProps.get("ASI_PREFIX");
        String suffix = userProps.get("ASI_SUFFIX");
        if (prefix != null && !prefix.isEmpty()) {
            if (prefix.indexOf("[") != 0) {
                retval.append("[" + prefix + "] ");
            }
        }
        retval.append(this.pbDesc);
        if (suffix != null && !suffix.isEmpty()) {
            if (suffix.indexOf("[") != 0) {
                retval.append(" [" + suffix + "]");
            }
        }
        return retval.toString();
    }

    @Override
    public void activate() {
        active = true;
    }

    @Override
    public String toStringFilter() {
        return getSystem() + " " + getIdentifier() + " " + getDeviceClass() + " " + getDeviceName() + " "
                + getProblemDescription();
    }

    @Override
    public String getTriplet() {
        return Triplet.create(faultFamily, faultMember, faultCode).toString();
    }

    @Override
    public long getUserTs() {
        return userTs;
    }

    public void setModeMask(String modeMask) {
        this.modeMask = modeMask;
    }
    
    @Override
    public ModeMask getModeMask() {
        if (modeMask == null || modeMask.isEmpty()) {
            return ModeMask.NONE;
        }
        return ModeMask.valueOf(modeMask);
    }

    @Override
    public String getPrefix() {
        return userProps.get("ASI_PREFIX");
    }

    @Override
    public String getSuffix() {
        return userProps.get("ASI_SUFFIX");
    }

    @Override
    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "ClientAlarmEvent [eventId=" + eventId + ", faultFamily=" + faultFamily + ", faultMember=" +
                faultMember + ", faultCode=" + faultCode + ", sourceId=" + sourceId + ", system=" + system +
                ", identifier=" + identifier + ", pbDesc=" + pbDesc + ", priority=" + priority + ", responsibleId=" +
                responsibleId + ", cause=" + cause + ", action=" + action + ", consequence=" + consequence +
                ", helpUrl=" + helpUrl + ", location=" + location + ", configId=" + configId + ", newFlag=" + newFlag +
                ", acknowledged=" + acknowledged + ", masked=" + masked + ", highlighted=" + highlighted +
                ", inhibited=" + inhibited + ", latched=" + latched + ", userProps=" + userProps + ", sourceTs=" +
                sourceTs + ", userTs=" + userTs + ", systemTs=" + systemTs + ", eventSeq=" + eventSeq +
                ", descriptor=" + descriptor + ", active=" + active + ", byBackup=" + byBackup + ", alarmId=" +
                alarmId + "]";
    }


    
}
