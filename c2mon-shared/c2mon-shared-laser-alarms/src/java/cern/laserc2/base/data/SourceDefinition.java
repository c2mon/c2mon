package cern.laserc2.base.data;

/***
 * An alarm source (member data), as well as the list of declared sources (class level). To obtain the list of active
 * surveillance alarms: getSources(), loop through the list and check isMissing() About the lastBackupTs: To avoid
 * triggering immediately a surveillance alarm for all sources at startup, the last backup received is considered to be
 * at the start time of the server. To really check if the source has sent a backup since the start of the server, one
 * must check the backupReceived flag!
 *
 * @version $Id: SourceDefinition.java 196628 2011-07-13 07:47:20Z mbuttner $
 * @author Mark Buttner
 */
public class SourceDefinition implements SourceHandle {
    /**
     * additional delay given to the source to send the expected signal (1.1 means 10% more than configured)
     */
    public static final double DELAY_FACTOR = 1.1;

    private String sourceId;
    private String sourceHost;
    private int backupTimeout;
    private boolean inConfig = false;
    private int responsibleId;

    private long lastBackupTs;
    private boolean backupMissing;

    private long lastMessageTs;

    private boolean keepAliveMissing;
    private long lastKeepAliveTs;
    private int keepAliveDelay;

    private long lastContactTs;

    //
    // --- CONSTRUCTION --------------------------------------------------------------
    //
    /**
     * Constructor.
     * 
     * @param sourceId <code>String</code> the name of the source as known by the LASER database
     * @param backupTimeout <code>int</code> the timeout in seconds
     */
    public SourceDefinition(String sourceId, int backupTimeout) {
        this.sourceId = sourceId;
        this.backupTimeout = backupTimeout;
        this.backupMissing = false;
    }

    public SourceDefinition(){
        // for jackson
    }

    //
    // --- PUBLIC METHOD -------------------------------------------------------------------
    //
    public void setResponsible(int rid) {
        this.responsibleId = rid;
    }

    @Override
    public int getResponsible() {
        return this.responsibleId;
    }

    /**
     * @return <code>String</code> name of the source as known in the LASER database
     */
    @Override
    public String getSourceId() {
        return this.sourceId;
    }

    /**
     * @return <code>String</code> hostname of the computer sending the backup message
     */
    @Override
    public String getSourceHost() {
        return this.sourceHost;
    }

    public void setSourceHost(String sourceHost) {
        this.sourceHost = sourceHost;
    }

    @Override
    public void receivedMessage() {
        this.lastMessageTs = System.currentTimeMillis();
    }

    @Override
    public long getLastMessageTs() {
        return this.lastMessageTs;
    }

    public void setLastMessageTs(long lastMessageTs) {
        this.lastMessageTs = lastMessageTs;
    }

    @Override
    public boolean isInConfig() {
        return inConfig;
    }

    @Override
    public void setInConfig(boolean inConfig) {
        this.inConfig = inConfig;
    }

    //
    // --- ABOUT BACKUPS -------------------------------------------------------------------
    //
    /***
     * Set properties of the source definition when a backup message is received.
     * 
     * @param host <code>String</code> hostname of the computer sending the backup message
     */
    @Override
    public void receivedBackup(String host) {
        this.sourceHost = host;
        this.lastBackupTs = System.currentTimeMillis();
        this.backupMissing = false;
    }

    /**
     * A 10% margin is given to the source to signal itself. If 0 the timestamp of last backup is initialized to avoid
     * to signal all sources in trouble right after boot of the server.
     */
    @Override
    public boolean checkForMissingBackup() {
        if (this.lastBackupTs == 0) {
            this.lastBackupTs = System.currentTimeMillis();
        }
        long currentTs = System.currentTimeMillis();
        if (backupTimeout > 0 && (currentTs - (backupTimeout * 1000 * DELAY_FACTOR) > lastBackupTs)) {
            return true;
        }
        return false;
    }

    /**
     * @return <code>boolean</code> current status of backup, true means surveillance alarm should be on
     */
    @Override
    public boolean isBackupMissing() {
        return this.backupMissing;
    }

    @Override
    public void setBackupMissing(boolean backupMissing) {
        this.backupMissing = backupMissing;
    }

    /**
     * @return <code>int</code> the backup timeout defined for this source, in seconds
     */
    @Override
    public int getBackupTimeout() {
        return this.backupTimeout;
    }

    @Override
    public long getLastBackupTs() {
        return this.lastBackupTs;
    }

    public void setLastBackupTs(long lastBackupTs) {
        this.lastBackupTs = lastBackupTs;
    }

    //
    // --- ABOUT KEEP ALIVE --------------------------------------------------------------
    //
    @Override
    public void receivedKeepAlive() {
        this.lastKeepAliveTs = System.currentTimeMillis();
    }

    @Override
    public boolean checkForMissingKeepAlive() {
        if (this.lastKeepAliveTs == 0) {
            this.lastKeepAliveTs = System.currentTimeMillis();
        }
        if (keepAliveDelay > 0) {
            if (System.currentTimeMillis() - (keepAliveDelay * 1000 * DELAY_FACTOR) > this.lastKeepAliveTs) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long getKeepAliveDelay() {
        return this.keepAliveDelay;
    }

    @Override
    public long getLastKeepAliveTs() {
        return this.lastKeepAliveTs;
    }

    public void setKeepAliveDelay(int keepAliveTimeout) {
        this.keepAliveDelay = keepAliveTimeout;
    }

    @Override
    public boolean isKeepAliveMssing() {
        return this.keepAliveMissing;
    }

    @Override
    public void setKeepAliveMissing(boolean keepAliveMissing) {
        this.keepAliveMissing = keepAliveMissing;
    }

    @Override
    public void setLastContactTs(long lastContactTs) {
        this.lastContactTs = lastContactTs;
    }

    @Override
    public long getLastContactTs() {
        return this.lastContactTs;
    }

}
