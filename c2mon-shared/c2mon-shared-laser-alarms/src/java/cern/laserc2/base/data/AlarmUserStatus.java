/**
 * Copyright (c) 2021 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

public class AlarmUserStatus {

    private long alarmId;
    private int configId;
    private boolean inhibited;
    private boolean masked;
    private boolean highlighted;
    private boolean latched;

    public AlarmUserStatus(){}

    public AlarmUserStatus(int selectedConfigId, long alarmId) {
        setConfigId(selectedConfigId);
        setAlarmId(alarmId);
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    public void setInhibited(boolean inhibited) {
        this.inhibited = inhibited;
    }

    public void setMasked(boolean masked) {
        this.masked = masked;
    }

    public long getAlarmId() {
        return this.alarmId;
    }

    public int getConfigId() {
        return this.configId;
    }

    public boolean isMasked() {
        return this.masked;
    }

    public boolean isHighlighted() {
        return this.highlighted;
    }

    public boolean isInhibited() {
        return this.inhibited;
    }

    public boolean isLatched() {
        return latched;
    }

    public void setLatched(boolean latched) {
        this.latched = latched;
    }

}
