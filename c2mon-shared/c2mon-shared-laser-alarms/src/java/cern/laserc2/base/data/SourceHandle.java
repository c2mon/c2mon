/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

/**
 * Interface for SourceDefinition.
 *
 * @author mbuttner
 */
public interface SourceHandle {
    String getSourceId();
    String getSourceHost();

    int getBackupTimeout();

    void setInConfig(boolean b);

    boolean isInConfig();

    long getLastMessageTs();

    long getLastBackupTs();

    long getLastKeepAliveTs();

    void receivedMessage();

    void receivedKeepAlive();

    void receivedBackup(String sourceHost);

    long getKeepAliveDelay();

    boolean isKeepAliveMssing();

    boolean checkForMissingKeepAlive();

    void setKeepAliveMissing(boolean keepAliveMissing);

    void setBackupMissing(boolean backupMissing);

    boolean isBackupMissing();

    boolean checkForMissingBackup();

    int getResponsible();

    void setLastContactTs(long lastContactTs);

    long getLastContactTs();

}
