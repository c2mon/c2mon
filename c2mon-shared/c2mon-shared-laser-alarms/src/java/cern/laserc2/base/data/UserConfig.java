/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

/**
 * Plain transfer object for the user configuration entry in the menu of the console. No logic attached other than
 * getters and setters (-> no need for unit tests)
 * 
 * @author mbuttner
 */
public class UserConfig {

    private int configId;
    private String userId;
    private String configName;
    private Boolean autoAck;
    private Boolean markAsNew;
    private int minPrio;
    private long lastUpdateTimestampMs;

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public Boolean getAutoAck() {
        return autoAck;
    }

    public void setAutoAck(Boolean autoAck) {
        this.autoAck = autoAck;
    }

    public Boolean getMarkAsNew() {
        return markAsNew;
    }

    public void setMarkAsNew(Boolean markAsNew) {
        this.markAsNew = markAsNew;
    }

    public int getMinPrio() {
        return minPrio;
    }

    public void setMinPrio(int minPrio) {
        this.minPrio = minPrio;
    }

    public long getLastUpdateTimestampMs() {
        return lastUpdateTimestampMs;
    }

    public void setLastUpdateTimestampMs(long lastUpdateTimestampMs) {
        this.lastUpdateTimestampMs = lastUpdateTimestampMs;
    }

}
