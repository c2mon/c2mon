/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

/**
 * Data structure required to decode the Json from the server for this data item.
 * 
 * @author mbuttner
 * @deprecated makes no sense with C2MON anymore. To be removed once we do not use legacy LASER anymore
 */
public class ServerStatus {
    private String hostname;
    private String appVersion;
    private String appName;
    private String startDate;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}
