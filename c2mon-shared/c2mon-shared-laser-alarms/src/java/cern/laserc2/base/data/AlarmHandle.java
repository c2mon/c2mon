/*
 * 
 * Copyright CERN, All Rights Reserved.
 */
package cern.laserc2.base.data;

/**
 * Interface for classes using the AlarmDefinition data.
 * 
 * @author mbuttner
 */
public interface AlarmHandle {
    enum SystemProperty {
        ASI_PREFIX,
        ASI_SUFFIX,
        ASI_EVENT_ID,
        PPM,
        DEVICE_TIMESTAMP,
        ASI_REASON
    }

    enum Descriptor {
        ACTIVE,
        TERMINATE,
        CHANGE,
        UNKNOWN_STATE
    }

    enum ModeMask {
        NONE,
        LATCH
    }

    long getEventId();

    String getDeviceClass();

    String getDeviceName();

    String getFaultCode();

    String getTriplet();

    long getAlarmId();

    int getPriority();

    String getProblemDescription();

    String getAction();

    String getConsequence();

    String getSystem();

    String getIdentifier();

    String getCause();

    String getHelpURL();

    void activate();

    String getSourceId();

    String getLocation();

    int getResponsibleId();

    String toStringFilter();

    long getUserTs();

    String getPrefix();

    String getSuffix();

    boolean isActive();

    boolean isMasked();

    boolean isInhibited();

    boolean isLatched();

    boolean isHighlighted();

    boolean isNewFlag();

    boolean isAcknowledged();

    long getSourceTs();

    long getSystemTs();

    void setNewFlag(boolean b);

    void setAcknowledged(boolean b);

    ModeMask getModeMask();

}