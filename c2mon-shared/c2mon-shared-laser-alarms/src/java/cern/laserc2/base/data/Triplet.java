/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.data;

/***
 * Simplify the building of the historical identifier of alarms, the triplet. This is the concatentation of fault
 * family, member and code separated by columns. Sometimes the code needs the parts of the identifier, sometimes the
 * concatenation of all.
 * <p>
 * Note that internally in the database, the alarm is identified by a sequence number. In the communication path source
 * API -> JMS -> server, the triplet string is used.
 * 
 * @author mbuttner
 */
public class Triplet {

    private String faultFamily;
    private String faultMember;
    private String faultCode;

    //
    // --- CONSTRUCTION -----------------------------------------------------------------------------
    //
    private Triplet(String triplet) {
        this.faultFamily = triplet.substring(0, triplet.indexOf(":"));
        this.faultMember = triplet.substring(triplet.indexOf(":") + 1, triplet.lastIndexOf(":"));
        this.faultCode = triplet.substring(triplet.lastIndexOf(":") + 1);
    }

    private Triplet(String faultFamily, String faultMember, String faultCode) {
        this.faultFamily = faultFamily;
        this.faultMember = faultMember;
        this.faultCode = faultCode;
    }

    public String getFaultFamily() {
        return faultFamily;
    }

    public String getFaultMember() {
        return faultMember;
    }

    public String getFaultCode() {
        return faultCode;
    }

    @Override
    public String toString() {
        return faultFamily + ":" + faultMember + ":" + faultCode;
    }

    //
    // --- STATIC -----------------------------------------------------------------------------
    //
    public static Triplet create(String triplet) {
        return new Triplet(triplet);
    }

    public static Triplet create(String faultFamily, String faultMember, int faultCode) {
        return new Triplet(faultFamily, faultMember, "" + faultCode);
    }

    public static Triplet create(String faultFamily, String faultMember, String faultCode) {
        return new Triplet(faultFamily, faultMember, faultCode);
    }

    //
    // --- Implements java.lang.Object ----------------------------------------------------------
    //
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((faultCode == null) ? 0 : faultCode.hashCode());
        result = prime * result + ((faultFamily == null) ? 0 : faultFamily.hashCode());
        result = prime * result + ((faultMember == null) ? 0 : faultMember.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Triplet other = (Triplet) obj;
        if (faultCode == null) {
            if (other.faultCode != null)
                return false;
        } else if (!faultCode.equals(other.faultCode))
            return false;
        if (faultFamily == null) {
            if (other.faultFamily != null)
                return false;
        } else if (!faultFamily.equals(other.faultFamily))
            return false;
        if (faultMember == null) {
            if (other.faultMember != null)
                return false;
        } else if (!faultMember.equals(other.faultMember))
            return false;
        return true;
    }

}
