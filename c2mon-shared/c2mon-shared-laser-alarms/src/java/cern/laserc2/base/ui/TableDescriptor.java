/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.ui;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

import cern.laserc2.base.data.AlarmHandle;

/**
 * A default table descriptor must be created for each configuration. The preference editor must allow to change the
 * parameters there. On connection, the default descriptor is copied to the session, this is the instance that is
 * changed. 
 * 
 * @author mbuttner
 */
public class TableDescriptor {
    // this list is based on the columns in use by our main clients, not the available in LASER
    public enum Column {
        DATE_TIME,
        BUILDING,
        SYSTEM_NAME,
        IDENTIFIER,
        FAULT_CODE,
        PROBLEM_DESCRIPTION,
        PRIORITY,
        SOURCE_ID,
        MNEMONIC,
        SITE
    }

    private static TableDescriptor defaultDescriptor;

    public static final int DEFAULT_WIDTH = 1350;
    public static final int DEFAULT_HEIGHT = 660;

    private int height = DEFAULT_HEIGHT;
    private int width = DEFAULT_WIDTH;

    private HashMap<Column, ColumnDescriptor> columns = new HashMap<>();

    //
    // --- CONSTRUCTION -------------------------------------------------------------------------------
    //
    public TableDescriptor() {
        int seq = 0;
        setColumn(Column.DATE_TIME, "Date/Time", true, 160, seq++);
        setColumn(Column.BUILDING, "Building", true, 80, seq++);
        setColumn(Column.SYSTEM_NAME, "System", true, 200, seq++);
        setColumn(Column.IDENTIFIER, "Identifier", true, 200, seq++);
        setColumn(Column.FAULT_CODE, "FC", true, 50, seq++);
        setColumn(Column.PROBLEM_DESCRIPTION, "Problem description", true, 500, seq++);
        setColumn(Column.PRIORITY, "Priority", false, 50, seq++);
        setColumn(Column.SOURCE_ID, "Source", false, 200, seq++);
        setColumn(Column.MNEMONIC, "Mnemonic", false, 80, seq++);
        setColumn(Column.SITE, "Site", false, 50, seq++);
    }

    public static TableDescriptor getDefaultDescriptor() {
        if (defaultDescriptor == null) {
            defaultDescriptor = new TableDescriptor();
        }
        return defaultDescriptor;
    }

    //
    // --- PUBLIC METHODS -----------------------------------------------------------------------------
    //
    public Collection<String> getColumnNames() {
        return Arrays.stream(Column.values()).map(Enum::name).collect(Collectors.toList());
    }

    /**
     * @return sorted list of displayed columns. This is what the pages should use for display!
     */
    public Collection<ColumnDescriptor> getColumns() {
        return columns.values().stream().filter(col -> col.isDisplayed())
                .sorted((c1, c2) -> c1.getDisplaySeq() - c2.getDisplaySeq()).collect(Collectors.toList());
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setColumn(Column col, String label, boolean display, int width, int seq) {
        ColumnDescriptor cd = columns.get(col);
        if (cd == null) {
            cd = new ColumnDescriptor(col);
            columns.put(col, cd);
        }
        cd.setLabel(label);
        cd.setColWidth(width);
        cd.setDisplayed(display);
        cd.setDisplaySeq(seq);
    }

    public boolean isDisplayed(String columnName) {
        ColumnDescriptor cd = columns.get(Column.valueOf(columnName));
        return (cd == null ? false : cd.isDisplayed());
    }

    public int getColumnSize(String columnName) {
        ColumnDescriptor cd = columns.get(Column.valueOf(columnName));
        return (cd == null ? 0 : cd.getColWidth());
    }

    public String getColumnLabel(String columnName) {
        ColumnDescriptor cd = columns.get(Column.valueOf(columnName));
        return (cd == null ? "" : cd.getLabel());
    }

    public String show(String columnName, AlarmHandle alarm) {
        switch (Column.valueOf(columnName)) {
        case BUILDING:
            return alarm.getLocation();
        case DATE_TIME:
            return ColumnDescriptor.DATE_FORMAT.format(new Date(alarm.getUserTs()));
        case FAULT_CODE:
            return "" + alarm.getFaultCode();
        case IDENTIFIER:
            return "<a href=\"" + alarm.getHelpURL() + "\" target=_blank>" + alarm.getIdentifier() + "</a>";
        case MNEMONIC:
            return "???";
        case PRIORITY:
            return "" + alarm.getPriority();
        case PROBLEM_DESCRIPTION:
            return "<a href=\"/details/alarm?alarm=" + alarm.getAlarmId() + "\">" + alarm.getPrefix() + " "
                    + alarm.getProblemDescription() + " " + alarm.getSuffix() + "</a>";
        case SITE:
            return "???";
        case SOURCE_ID:
            return alarm.getSourceId();
        case SYSTEM_NAME:
            return alarm.getSystem();
        default:
            break;
        }
        return "";
    }

    public int getHeight() {
        return this.height;
    }

    public int getBodyHeight() {
        return getHeight() - 60;
    }

    public int getWidth() {
        return this.width;
    }
}
