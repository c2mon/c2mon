/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.ui;

import java.text.SimpleDateFormat;

import cern.laserc2.base.ui.TableDescriptor.Column;

/*-
 * Adds to a given column in the display (either Swing or Web) information about the size,
 * sequence in the table, label, etc.
 * 
 * The default date format for GUI display is also defined here (perhaps not the best place). 
 * @author mbuttner
 */
public class ColumnDescriptor
{
    public static final String DATE_FORMAT_PROP = "laserc2.format.date";
    public static final String DFT_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
    
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(System.getProperty(DATE_FORMAT_PROP,
            DFT_DATE_FORMAT));

    
    private Column id;
    private String label;
    private boolean displayed;
    private int colWidth;
    private int displaySeq;

    //
    // --- CONSTRUCTION ---------------------------------------------------------------------
    //
    public ColumnDescriptor(Column id)
    {
        this.id = id;
    }

    //
    // --- PUBLIC METHODS -------------------------------------------------------------------
    //
    public Column getId()
    {
        return id;
    }

    public String getName()
    {
        return id.toString();
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public boolean isDisplayed()
    {
        return displayed;
    }

    public void setDisplayed(boolean displayed)
    {
        this.displayed = displayed;
    }

    public int getColWidth()
    {
        return colWidth;
    }

    public void setColWidth(int colWidth)
    {
        this.colWidth = colWidth;
    }

    public int getDisplaySeq()
    {
        return displaySeq;
    }

    public void setDisplaySeq(int displaySeq)
    {
        this.displaySeq = displaySeq;
    }
}