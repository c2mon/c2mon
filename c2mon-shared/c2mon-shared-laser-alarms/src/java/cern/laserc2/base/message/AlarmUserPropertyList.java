/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user-properties")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class AlarmUserPropertyList {
    private ArrayList<AlarmUserProperty> properties = new ArrayList<>();

    public void addProperty(String key, String value) {
        properties.add(new AlarmUserProperty(key, value));
    }

    @XmlElement(name = "property")
    public ArrayList<AlarmUserProperty> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<AlarmUserProperty> properties) {
        this.properties = properties;
    }

    /**
     * Produce the DB compatible string representation of a set of user properties, i.e. key1=value1@@key2=value2@@ ...
     */
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        boolean firstEntry = true;
        for (AlarmUserProperty prop : properties) {
            if (prop.getValue() != null && !prop.getValue().isEmpty()) {
                if (!firstEntry) {
                    buf.append("@@");
                } else {
                    firstEntry = false;
                }
                buf.append(prop.getName() + "=" + prop.getValue());
            }
        }
        return buf.toString();
    }
}
