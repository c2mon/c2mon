/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "seconds", "microseconds" })
public class AlarmTimestamp {

    private long seconds;
    private long microseconds;

    //
    // --- CONSTRUCTION -----------------------------------------------------
    //
    public AlarmTimestamp() {

    }

    public AlarmTimestamp(long millis) {
        this.seconds = millis / 1000;
        this.microseconds = (millis - seconds * 1000) * 1000;
    }

    //
    // --- PUBLIC METHODS ---------------------------------------------------
    //
    @XmlAttribute(name = "seconds")
    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    @XmlAttribute(name = "microseconds")
    public long getMicroseconds() {
        return microseconds;
    }

    public void setMicroseconds(long microseconds) {
        this.microseconds = microseconds;
    }

    @Override
    public String toString() {
        return (new Date(getMillis())).toString();
    }

    public long getMillis() {
        return seconds * 1000 + microseconds / 1000;
    }

}
