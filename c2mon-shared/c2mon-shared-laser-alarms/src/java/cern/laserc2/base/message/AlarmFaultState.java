/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.Map;

public interface AlarmFaultState {
    public enum AlarmFaultStateDescriptor {
        ACTIVE,
        TERMINATE,
        CHANGE
    }

    //
    // --- STATIC -----------------------------------------------------------------
    //

    /**
     * The build method used by Source to create the copies of AlarmHandles needed later on for asynchronous processing
     */
    static AlarmFaultState createFromAlarm(AlarmCacheStatus alarm, AlarmFaultStateDescriptor descriptor) {
        AlarmFaultState fs = new AlarmFaultStateImpl(alarm.getTriplet(), descriptor);
//        if (descriptor != AlarmFaultStateDescriptor.TERMINATE) {
            Map<String, String> originalProps = ((AlarmCacheStatusImpl) alarm).getProperties();
            for (String key : originalProps.keySet()) {
                fs.addUserProperty(key, originalProps.get(key));
            }
//        }
        fs.setUserTs(alarm.getUserTs());
        return fs;
    }

    void setUserTs(long currentTimeMillis);

    void addUserProperty(String key, String value);

    String getTriplet();

    AlarmFaultStateDescriptor getDescriptor();

    void setDescriptor(AlarmFaultStateDescriptor descriptor);

    AlarmTimestamp getUserTs();

    boolean isActive();

    AlarmUserPropertyList getProperties();

    void clearProperties();
    
    // APS-7350 for GM alarm redirection
    void setCode(String faultCode);
}
