/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class AlarmMessageFactoryImpl implements AlarmMessageFactory {
    private Jaxb2Marshaller marshaller;

    //
    // --- CONSTRUCTION -----------------------------------------------------------------------------
    //
    public AlarmMessageFactoryImpl() {
        marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("cern.laserc2.base.message");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jaxb.formatted.output", true);
        marshaller.setMarshallerProperties(map);
    }

    //
    // --- Implements AlarmMessageFactory ------------------------------------------------------------
    //
    @Override
    public void writeMessageToXmlFile(String fileName, AlarmMessage alarmMessage)
            throws FileNotFoundException, IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            marshaller.marshal(alarmMessage, new StreamResult(fos));
        }
    }

    @Override
    public String alarmMessageToXml(AlarmMessage graph) throws IOException {
        try (StringWriter fos = new StringWriter()) {
            marshaller.marshal(graph, new StreamResult(fos));
            return fos.toString();
        }
    }

    @Override
    public AlarmMessage createMessagefromXml(String xml) throws IOException {
        AlarmMessageImpl message = (AlarmMessageImpl) marshaller.unmarshal(new StreamSource(new StringReader(xml)));
        message.setXmlSourceCode(xml);
        return message;
    }

    @Override
    public AlarmMessage createMessagefromXmlFile(String filename) throws IOException {
        try (FileInputStream fis = new FileInputStream(filename)) {
            AlarmMessageImpl message = (AlarmMessageImpl) marshaller.unmarshal(new StreamSource(fis));
            message.setXmlSourceCode(filename);
            return message;
        }
    }

    @Override
    public AlarmMessage createMessage(String sourceId, boolean isBackup) {
        AlarmMessageImpl message = new AlarmMessageImpl();
        message.setBackup(isBackup);
        message.setSourceName(sourceId);
        try {
            message.setSourceHostname(java.net.InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            message.setSourceHostname("undefined");
        }
        message.setSourceTimeStamp(new AlarmTimestamp(System.currentTimeMillis()));
        message.setVersion("1.1");
        return message;
    }

}
