/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

public class MessageHeaders {

    public static final String LAST_EVENT = "LASER_LW_LAST_EVENT";
    public static final String SERVER_BOOT_TS = "LASER_LW_SERVER_BOOT_TS";
    
    private MessageHeaders() {

    }
}
