/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.HashMap;

import cern.laserc2.base.data.Triplet;

/**
 * Current state of the alarm in the cache of a LASER-lw application (which is close but different from the
 * "state change" as described by the AlarmFaultState object).
 * 
 * @author mbuttner
 */
public interface AlarmCacheStatus {

    /**
     * @return a new instance of the implementation provided for this interface
     */
    public static AlarmCacheStatus createCacheStatus(String faultFamily, String faultMember, String faultCode) {
        return new AlarmCacheStatusImpl(Triplet.create(faultFamily, faultMember, faultCode).toString());
    }

    /**
     * @deprecated use getTriplet() instead
     */
    @Deprecated
    String getAlarmId();

    String getTriplet();

    long getUserTs();

    long getSystemTs();

    boolean isActive();

    void clearProperties();

    void setProperty(String key, String value);

    void setSystemTs(long systemTs);

    void setProperties(HashMap<String, String> propertyMap);
}
