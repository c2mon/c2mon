/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface AlarmMessageFactory
{
    public static AlarmMessageFactory getFactory()
    {
        return new AlarmMessageFactoryImpl();
    }

    AlarmMessage createMessagefromXmlFile(String filename) throws IOException;
    AlarmMessage createMessagefromXml(String xml) throws IOException;
    AlarmMessage createMessage(String sourceId, boolean isBackup);

    void writeMessageToXmlFile(String filename, AlarmMessage alarmMessage) throws FileNotFoundException, IOException;
    String alarmMessageToXml(AlarmMessage amsg) throws IOException;
 
}
