/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.HashMap;
import java.util.Map;

public class AlarmCacheStatusImpl implements AlarmCacheStatus {
    private String triplet;
    private long userTs;
    private long systemTs;
    private boolean active;

    private HashMap<String, String> userProperties = new HashMap<>();

    /**
     * @param string
     */
    public AlarmCacheStatusImpl(String alarmId) {
        this.triplet = alarmId;
    }

    @Override
    @Deprecated
    public String getAlarmId() {
        return triplet;
    }

    @Deprecated
    void setAlarmId(String alarmId) {
        this.triplet = alarmId;
    }

    @Override
    public String getTriplet() {
        return triplet ;
    }

    public void setTriplet(String triplet) {
        this.triplet = triplet;
    }
    
    @Override
    public long getUserTs() {
        return userTs;
    }

    public void setUserTs(long userTs) {
        this.userTs = userTs;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if (active != this.active) {
            systemTs = System.currentTimeMillis();
        }
        this.active = active;
    }

    @Override
    public void clearProperties() {
        userProperties.clear();
    }

    @Override
    public void setProperty(String key, String value) {
        userProperties.put(key, value);
    }

    protected Map<String, String> getProperties() {
        return userProperties;
    }

    @Override
    public void setSystemTs(long systemTs) {
        this.systemTs = systemTs;
    }

    @Override
    public long getSystemTs() {
        return systemTs;
    }

    @Override
    public void setProperties(HashMap<String, String> propertyMap) {
        if (propertyMap != null) {
            this.userProperties = propertyMap;
        } else {
            this.userProperties.clear();
        }
    }

}
