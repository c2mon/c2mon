/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import cern.laserc2.base.data.Triplet;

@XmlRootElement(name = "fault-state")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlType(propOrder = { "family", "member", "code", "descriptor", "userTimeStamp", "properties" })
public class AlarmFaultStateImpl implements AlarmFaultState {

    private String family;
    private String member;
    private String code;
    private AlarmFaultStateDescriptor descriptor;
    private AlarmTimestamp userTimeStamp;
    private AlarmUserPropertyList properties;

    //
    // --- CONSTRUCTION ------------------------------------------------------------------
    //
    public AlarmFaultStateImpl() {
        this.properties = new AlarmUserPropertyList();
    }

    public AlarmFaultStateImpl(String family, String member, String code, AlarmFaultStateDescriptor descriptor) {
        this.family = family;
        this.member = member;
        this.code = code;
        this.descriptor = descriptor;
        this.userTimeStamp = new AlarmTimestamp(System.currentTimeMillis());
        this.properties = new AlarmUserPropertyList();
    }

    public AlarmFaultStateImpl(String alarmId, AlarmFaultStateDescriptor descriptor) {
        Triplet triplet = Triplet.create(alarmId);
        this.family = triplet.getFaultFamily();
        this.member = triplet.getFaultMember();
        this.code = triplet.getFaultCode();
        this.descriptor = descriptor;
        this.userTimeStamp = new AlarmTimestamp(System.currentTimeMillis());
        this.properties = new AlarmUserPropertyList();
    }

    //
    // --- PUBLIC METHODS ------------------------------------------------------------------
    //
    @XmlAttribute(name = "family")
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @XmlAttribute(name = "member")
    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    @XmlAttribute(name = "code")
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    @XmlElement(name = "descriptor")
    public AlarmFaultStateDescriptor getDescriptor() {
        return descriptor;
    }

    @Override
    public void setDescriptor(AlarmFaultStateDescriptor descriptor) {
        this.descriptor = descriptor;
    }


    @Override
    @XmlElement(name = "user-properties")
    public AlarmUserPropertyList getProperties() {
        return properties;
    }

    public void setProperties(AlarmUserPropertyList properties) {
        this.properties = properties;
    }

    @Override
    public void setUserTs(long uts) {
        this.setUserTimeStamp(new AlarmTimestamp(uts));
    }

    /* use getUSerTimeStamp() instead */
    @Deprecated
    @Override
    public AlarmTimestamp getUserTs() {
        return this.userTimeStamp;
    }

    @XmlElement(name = "user-timestamp")
    public AlarmTimestamp getUserTimeStamp() {
        return userTimeStamp;
    }

    public void setUserTimeStamp(AlarmTimestamp userTimeStamp) {
        this.userTimeStamp = userTimeStamp;
    }

    
    @Override
    public void addUserProperty(String key, String value) {
        properties.addProperty(key, value);
    }

    @Override
    public String getTriplet() {
        return this.family + ":" + this.member + ":" + this.getCode();
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        if (properties != null) {
            buffer.append("[");
            for (AlarmUserProperty prop : properties.getProperties()) {
                buffer.append(prop.getName());
                buffer.append("=");
                buffer.append(prop.getValue());
                buffer.append(";");
            }
            buffer.append("]");
        }
        return "AlarmFaultStateImpl [" + (family != null ? "family=" + family + ", " : "")
                + (member != null ? "member=" + member + ", " : "") + "code=" + code + ", "
                + (descriptor != null ? "descriptor=" + descriptor + ", " : "")
                + (userTimeStamp != null ? "userTimeStamp=" + userTimeStamp + ", " : "")
                + (properties != null ? "properties=" + buffer.toString() : "") + "]";
    }

    @Override
    public boolean isActive() {
        return (this.descriptor != AlarmFaultStateDescriptor.TERMINATE);
    }

    @Override
    public void clearProperties() {
        properties.getProperties().clear();
    }

}
