/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor;

@XmlRootElement(name = "ASI-message")
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(propOrder = { "version", "backup", "sourceName", "sourceHostname", "sourceTimeStamp", "faultStates" })
public class AlarmMessageImpl implements AlarmMessage {

    private boolean backup;
    private String version;
    private String sourceName;
    private String sourceHostname;
    private AlarmTimestamp sourceTimeStamp;
    private AlarmFaultStateList faultStates = new AlarmFaultStateList();

    private transient String xmlOrigin;

    @Override
    @XmlAttribute(name = "backup")
    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        this.backup = backup;
    }

    @XmlElement(name = "source-name")
    public String getSourceName() {
        return sourceName;
    }

    // synonym for getSourceName, for backward compat.
    @Override
    public String getSourceId() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    @Override
    @XmlElement(name = "source-hostname")
    public String getSourceHostname() {
        return sourceHostname;
    }

    public void setSourceHostname(String sourceHostName) {
        this.sourceHostname = sourceHostName;
    }

    @Override
    @XmlElement(name = "source-timestamp")
    public AlarmTimestamp getSourceTimeStamp() {
        return sourceTimeStamp;
    }

    public void setSourceTimeStamp(AlarmTimestamp sourceTimeStamp) {
        this.sourceTimeStamp = sourceTimeStamp;
    }

    @Override
    @XmlElement(name = "fault-states")
    public AlarmFaultStateList getFaultStates() {
        return faultStates;
    }

    public void setFaultStates(AlarmFaultStateList faultStates) {
        this.faultStates = faultStates;
    }

    @XmlAttribute
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public void addFaultState(AlarmFaultState afs) {
        this.faultStates.add(afs);
    }

    @Override
    public void addFaultStates(Collection<AlarmFaultState> moreFaultStates) {
        faultStates.add(moreFaultStates);
        // for (AlarmFaultState afs : moreFaultStates) {
        // addFaultState(afs);
        // }
    }

    @Override
    public void addFaultState(String ff, String fm, String fc, AlarmFaultStateDescriptor descriptor) {
        this.addFaultState(new AlarmFaultStateImpl(ff, fm, fc, descriptor));
    }

    public void setXmlSourceCode(String sourceCode) {
        this.xmlOrigin = sourceCode;
    }

    /**
     * @return <code>String</code> Might be either the name of the file or the XML code depending on how the message was
     *         created
     */
    @Override
    public String getXmlSourceCode() {
        return xmlOrigin;
    }

    @Override
    public boolean isKeepAlive() {
        return !isBackup() && (this.faultStates == null ||this.faultStates.isEmpty());
    }
}
