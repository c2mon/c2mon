/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.Collection;

import cern.laserc2.base.message.AlarmFaultState.AlarmFaultStateDescriptor;

public interface AlarmMessage
{
    void addFaultState(AlarmFaultState afs);
    void addFaultState(String ff, String fm, String fc, AlarmFaultStateDescriptor descriptor);
    void addFaultStates(Collection<AlarmFaultState> faultStates);

    String getSourceHostname();
    boolean isBackup();
    boolean isKeepAlive();
    String getSourceId();
    AlarmTimestamp getSourceTimeStamp();
    AlarmFaultStateList getFaultStates();
    
    String getXmlSourceCode();
}
