/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

public class SourceState {

    private String sourceId;
    private long sourceTs;
    private String sourceHostName;
    private boolean backup;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public long getSourceTs() {
        return sourceTs;
    }

    public void setSourceTs(long sourceTs) {
        this.sourceTs = sourceTs;
    }

    public String getSourceHostName() {
        return sourceHostName;
    }

    public void setSourceHostName(String sourceHostName) {
        this.sourceHostName = sourceHostName;
    }

    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        this.backup = backup;
    }

    @Override
    public String toString() {
        return "SourceState [sourceId=" + sourceId + ", sourceTs=" + sourceTs + ", sourceHostName="
                + sourceHostName + ", backup=" + backup + "]";
    }

    
    
}
