/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.message;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fault-states")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)

public class AlarmFaultStateList {
    private ArrayList<AlarmFaultStateImpl> faultStates = new ArrayList<>();

    @XmlElement(name = "fault-state")
    public Collection<AlarmFaultStateImpl> getFaultStates() {
        return faultStates;
    }

    public void add(AlarmFaultState afs) {
        faultStates.add((AlarmFaultStateImpl) afs);
    }
    
    public void add(Collection<AlarmFaultState> moreFaultStates) {
        for (AlarmFaultState state : moreFaultStates) {
            faultStates.add((AlarmFaultStateImpl)state);
        }
    }
    
    public boolean isEmpty() {
        return faultStates.size() == 0;
    }
}
