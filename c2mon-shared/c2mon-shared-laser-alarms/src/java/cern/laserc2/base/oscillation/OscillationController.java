/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.oscillation;

import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource
public class OscillationController {

    @Value("${laserc2.hist.oscillation_control_enabled:true}")
    boolean oscillationControlEnabled;

    private static final OscillationData EMPTY = new OscillationData(0);

    // Key is the alarm id triplet
    private ConcurrentHashMap<String, OscillationData> oscillationData = new ConcurrentHashMap<>();
    private HashSet<String> oscillatingAlarmIds = new HashSet<>();

    private int oscillatingAlarms;

    //
    // --- PUBLIC METHODS ----------------------------------------------------
    //
    public void setOscillationNotified(OscillationData osc, boolean oscillationNotified) {
        osc.setOscillationNotified(oscillationNotified);
    }

    public boolean isOscillationNotified(OscillationData osc) {
        return osc.isOscillationNotified();
    }

    public synchronized void reset(String alarmId) {
        oscillationData.put(alarmId, new OscillationData(System.currentTimeMillis()));
    }
    
    public synchronized OscillationData recordEvent(String alarmId, long refTs) {
        if (this.oscillationControlEnabled) {
            OscillationData osc = oscillationData.get(alarmId);
            if (osc == null) {
                osc = new OscillationData(refTs);
                oscillationData.put(alarmId, osc);
            } else {
                osc.recordEvent(refTs);
            }
            if (osc.isOscillating()) {
                this.oscillatingAlarmIds.add(alarmId);
            } else {
                this.oscillatingAlarmIds.remove(alarmId);
            }
            oscillatingAlarms = oscillatingAlarmIds.size();
            return osc;
        }
        return EMPTY;
    }

    public boolean isOscillating(OscillationData osc) {
        return osc.isOscillating();
    }

   
    //
    // --- Remote control ---------------------------------------------------------------
    //
    @ManagedOperation
    public void enableOscillationControl() {
        this.oscillationControlEnabled = true;
    }

    @ManagedOperation
    public void disableOscillationControl() {
        this.oscillationControlEnabled = false;
    }

    @ManagedAttribute
    public boolean isOscillationControlEnabled() {
        return this.oscillationControlEnabled;
    }

    @ManagedOperation
    public void setMaxAllowedEvents(int events) throws Exception {
        if (events > OscillationData.OSCILLATION_BUFFER_SIZE || events < 3) {
            throw new IllegalArgumentException(
                    "Allowed number of events is between 3 and " + OscillationData.OSCILLATION_BUFFER_SIZE);
        }
        OscillationData.maxAllowedEvents = events;
    }

    @ManagedAttribute
    public int getMaxAllowedEvents() {
        return OscillationData.maxAllowedEvents;
    }

    @ManagedAttribute
    public int getOscillatingCount() {
        return this.oscillatingAlarms;
    }

    @ManagedOperation
    public void setLockoutDelay(int minutes) {
        if (minutes < 3) {
            throw new IllegalArgumentException("Minimum lockout time is 3 minutes");
        }
        OscillationData.lockOutDelay = minutes;
    }

    @ManagedAttribute
    public int getPeriod() {
        return OscillationData.oscillationPeriod;
    }

    @ManagedAttribute
    public int getLockoutDelay() {
        return OscillationData.lockOutDelay;
    }

    @ManagedOperation
    public void setPeriod(int minutes) {
        if (minutes < 1) {
            throw new IllegalArgumentException("Minimum control period id 1 minute");
        }
        OscillationData.oscillationPeriod = minutes;
    }

}
