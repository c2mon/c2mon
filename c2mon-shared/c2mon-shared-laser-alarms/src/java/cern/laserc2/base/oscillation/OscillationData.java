/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base.oscillation;

public class OscillationData {
    public static final int OSCILLATION_BUFFER_SIZE = 30;
    public static final long ONE_HOUR = 60 * 60 * 1000;
    public static final long MINUTE = 60 * 1000;

    // periods in minutes. The default configuration means: If more than 15 events occur
    // for the same alarm withing 5 minutes, no event will be recorded to history during
    // the following 55 minutes
    static int oscillationPeriod = 5;
    static int maxAllowedEvents = 15;
    static int lockOutDelay = 55;

    private boolean oscillationNotified;
    private boolean isOscillating;
    private long oscillationStartTs;
    private long[] oscillations = new long[OSCILLATION_BUFFER_SIZE];

    //
    // --- CONSTRUCTION ---------------------------------------------------------
    //
    /**
     * Create a new oscillation data record, with the timestamp of the initial event. The parameter is explicit to allow
     * test automation (which would be more difficult using always System.currentMillis().
     * 
     * @param ts initial timestamp in millis
     */
    OscillationData(long ts) {
        oscillations[0] = ts;
    }

    /**
     * Replace the lowest value in the array with the new incoming. Compute status
     * of alarm (oscillating or not).
     *
     * @param ts
     */
    synchronized void recordEvent(long ts) {
        boolean stored = false;
        int oldestIndex = -1;
        long oldestTs = -1;
        
        // insert the new event at the place of the oldest already known (circ. buffer).
        for (int i = 0; i < OSCILLATION_BUFFER_SIZE; i++) {
            if (oscillations[i] == 0) {
                oscillations[i] = ts;
                stored = true;
                break;
            }
            if (oscillations[i] < oldestTs || oldestTs == -1) {
                oldestTs = oscillations[i];
                oldestIndex = i;
            }
        }
        if (!stored) {
            oscillations[oldestIndex] = ts;
        }

        // if the status is already considered to be "oscillating", make sure the
        // lockout delay is not yet over (or mark as not oscillating anymore),
        if (isOscillating) {
            // check that the oscillation start is more than 1 hour ago
            if (oscillationStartTs < ts - (lockOutDelay * MINUTE)) {
                isOscillating = false;
            }
        } else {
            int oscCounter = 0;
            for (int i = 0; i < OSCILLATION_BUFFER_SIZE; i++) {
                if (oscillations[i] >= ts - (oscillationPeriod * MINUTE)) {
                    oscCounter++;
                }
            }
            if (oscCounter >= maxAllowedEvents) {
                oscillationStartTs = ts;
                oscillationNotified = false;
                isOscillating = true;
            }
        }
    }

    boolean isOscillationNotified() {
        return oscillationNotified;
    }

    void setOscillationNotified(boolean oscillationNotified) {
        this.oscillationNotified = oscillationNotified;
    }

    boolean isOscillating() {
        return isOscillating;
    }

    void setOscillating(boolean isOscillating) {
        this.isOscillating = isOscillating;
    }
}
