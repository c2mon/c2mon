/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.laserc2.base;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.Objects;

import org.slf4j.LoggerFactory;

public class BootLock {

    public static final String BOOT_LOCK_FILE = "/tmp/laser-lw-boot.lock";

    private BootLock() {

    }

    /**
     * @return <code>boolean</code> true in case the lock file was created, false otherwise
     */
    public static boolean touchLock() {
        try {
            Path path = FileSystems.getDefault().getPath(BOOT_LOCK_FILE);
            Objects.requireNonNull(path, "path is null");
            if (Files.exists(path)) {
                Files.setLastModifiedTime(path, FileTime.from(Instant.now()));
            } else {
                Files.createFile(path);
                return true;
            }
        } catch (IOException e) {
            LoggerFactory.getLogger(BootLock.class).warn("Failed to update ts of boot lock file, ignored", e);
        }
        return false;
    }

    /**
     * @param seconds <code>int</code> the number of seconds to wait after last update of the lock file
     * @return <code>long</code> the number of milliseconds slept before returning, or -1 in case of exception
     */
    public static long waitForBootLock(int seconds) {
        try {
            Path path = FileSystems.getDefault().getPath(BOOT_LOCK_FILE);
            Objects.requireNonNull(path, "path is null");
            if (Files.exists(path)) {
                boolean cont = true;
                int loopCounter = 0;
                long totalWaitTime = 0;
                while (cont && loopCounter < 3) {
                    loopCounter++;
                    FileTime ts = Files.getLastModifiedTime(path);
                    long waitTime = ts.toMillis() + (seconds * 1000) - System.currentTimeMillis();
                    if (waitTime > 0) {
                        LoggerFactory.getLogger(BootLock.class).info("Broker not ready, waiting {}ms ...", waitTime);
                        Thread.sleep(waitTime);
                        totalWaitTime += waitTime;
                    }
                }
                return totalWaitTime;
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(BootLock.class).warn("Failed to wait {} for boot lock, ignored", e);
            return -1;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println("Starting the test ...");
        System.out.println("Creating or updating the bootlock file (" + BootLock.touchLock() + ") ... ");
        System.out.println("Waiting 10s for the lock file since last update ...");
        System.out.println("Effective wait time: " + BootLock.waitForBootLock(10) + "ms.");
        System.out.println("Ok.");
    }

}
