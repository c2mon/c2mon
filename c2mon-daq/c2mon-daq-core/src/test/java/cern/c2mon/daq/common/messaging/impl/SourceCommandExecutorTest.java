/******************************************************************************
 * Copyright (C) 2010-2016 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.daq.common.messaging.impl;

import static cern.c2mon.shared.daq.command.SourceCommandTagReport.Status.STATUS_NOK_FROM_EQUIPMENTD;
import static cern.c2mon.shared.daq.command.SourceCommandTagReport.Status.STATUS_NOK_TIMEOUT;
import static cern.c2mon.shared.daq.command.SourceCommandTagReport.Status.STATUS_OK;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.Before;
import org.junit.Test;

import cern.c2mon.daq.common.ICommandRunner;
import cern.c2mon.daq.tools.equipmentexceptions.EqCommandTagException;
import cern.c2mon.shared.daq.command.SourceCommandTagValue;

public class SourceCommandExecutorTest {

  private SourceCommandTagValue command;
  private ICommandRunner commandRunner;

  @Before
  public void setUp() {
    command = new SourceCommandTagValue(1L, "name", 1L, (short) 1, "asd", "Boolean");
    commandRunner = createMock(ICommandRunner.class);
  }

  @Test
  public void testCallSuccess() throws EqCommandTagException {
    SourceCommandExecutor executor = new SourceCommandExecutor(commandRunner, command);
    commandRunner.runCommand(command);
    expectLastCall().andReturn("");
    replay(commandRunner);
    executor.start();

    Awaitility.await().with().pollDelay(100, TimeUnit.MILLISECONDS).atMost(1, TimeUnit.SECONDS).until(() ->
            executor.getSourceCommandTagReport().getStatus().equals(STATUS_OK) &&
                    executor.getSourceCommandTagReport().getDescription() == null);
    verify(commandRunner);
  }

  @Test
  public void testCallSuccessReturnHello() {
    final String message = "hello";
    SourceCommandExecutor executor = new SourceCommandExecutor(sourceCommandTagValue -> message, command);
    executor.start();
    Awaitility.await()
            .with().pollDelay(20, TimeUnit.MILLISECONDS)
            .atMost(1, TimeUnit.SECONDS)
            .until(() ->
                    executor.getSourceCommandTagReport().getStatus() == STATUS_OK &&
                    executor.getSourceCommandTagReport().getReturnValue().equals(message));
  }

  @Test
  public void testCallNoResponse() {
    SourceCommandExecutor executor = new SourceCommandExecutor(sourceCommandTagValue -> {
      try {
        Thread.sleep(Long.MAX_VALUE);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return "unimportant";
    }, command);
    executor.start();
    Awaitility.await().with().pollDelay(10, TimeUnit.MILLISECONDS)
            .atMost(1, TimeUnit.SECONDS)
            .until(() -> executor.getSourceCommandTagReport().getStatus() == STATUS_NOK_TIMEOUT);
  }

  @Test
  public void testException() {
    SourceCommandExecutor executor = new SourceCommandExecutor(sourceCommandTagValue -> {
      throw new RuntimeException();
    }, command);
    executor.start();
    Awaitility.await().with().pollDelay(50, TimeUnit.MILLISECONDS)
            .atMost(1, TimeUnit.SECONDS)
            .until(() -> executor.getSourceCommandTagReport().getStatus() == STATUS_NOK_FROM_EQUIPMENTD);
  }
}
